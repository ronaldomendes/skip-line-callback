package com.cursospring.batch.skiplinecallback;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableBatchProcessing
@ComponentScan(basePackages = {"com.cursospring.batch.skiplinecallback"})
public class SkipLineCallbackApplication {

    public static void main(String[] args) {
        SpringApplication.run(SkipLineCallbackApplication.class, args);
    }

}
