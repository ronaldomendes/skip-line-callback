package com.cursospring.batch.skiplinecallback.processor;

import com.cursospring.batch.skiplinecallback.dto.EmployeeDTO;
import com.cursospring.batch.skiplinecallback.model.Employee;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import java.util.Random;

@Component
@Slf4j
public class EmployeeProcessor implements ItemProcessor<EmployeeDTO, Employee> {

    @Override
    public Employee process(EmployeeDTO dto) throws Exception {
        Employee employee = new Employee();
        employee.setEmployeeId(dto.getEmployeeId() + new Random().nextInt(1000000));
        employee.setFirstName(dto.getFirstName());
        employee.setLastName(dto.getLastName());
        employee.setEmail(dto.getEmail());
        employee.setAge(dto.getAge());
        log.info("Inside processor: {}", employee.toString());

        return employee;
    }
}
