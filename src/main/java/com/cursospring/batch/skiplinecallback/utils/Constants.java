package com.cursospring.batch.skiplinecallback.utils;

public class Constants {

    public static final String CONTEXT_KEY_NAME = "fileName";
    public static final String FILE_NAME_CSV = "employees.csv";
    public static final String EMPLOYEE_ID = "employeeId";
    public static final String FIRSTNAME = "firstName";
    public static final String LASTNAME = "lastName";
    public static final String EMAIL = "email";
    public static final String AGE = "age";
    public static final String QUALIFIER_NAME = "demoJob";
    public static final String STEP_NAME = "stepOne";
    public static final int CHUNK_SIZE = 10;

    private Constants() {
    }
}
